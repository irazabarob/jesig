const storeProducto = "";
let botonPrev;
let botonNext;

let parametroBusqueda;

//variables para la paginacion
let pageNumber = 1;
const pageSize = 9;
let page;

window.onload = () => {
    fetch('http://127.0.0.1:9090/magneto-jlig/producto/lista')
        .then(res => res.ok ? Promise.resolve(res) : Promise.reject(res))
        .then(res => res.json())
        .then(res => {

            this.storeProducto = res;
            const listaProducto = document.getElementById('listaProducto');
            const fragment = document.createDocumentFragment();

            inicializarDatos();
            
            for (const producto of this.storeProducto) {
                cardProducto = dibujarCard(producto);
                fragment.appendChild(cardProducto);
            }
        
            listaProducto.appendChild(fragment);
            showItems();
        });
}

const inicializarDatos = () => {
    botonPrev = document.querySelector('.prev');
    botonNext = document.querySelector('.next');
    checkPaginador();
    this.page = document.querySelector('.page');
    this.parametroBusqueda = document.querySelector(".busqueda");
}

const dibujarCard = (producto) => {
    const cardCreado = document.createElement('DIV');
    cardCreado.classList.add('card');

    const foto = buildFoto(producto);
    const descripcion = buildDescripcion(producto);

    cardCreado.appendChild(foto);
    cardCreado.appendChild(descripcion);

    return cardCreado;
}

const buildFoto = (producto) => {
    const itemCreado = document.createElement('DIV');
    const imgCreado = document.createElement('IMG');

    imgCreado.src = producto.foto;

    itemCreado.classList.add('item');
    itemCreado.appendChild(imgCreado);

    return itemCreado;
}

const buildDescripcion = (producto) => {
    const descripcionCreado = document.createElement('DIV');
    const stok = document.createElement('LABEL');
    const descripcion = document.createElement('P');
    const precio = document.createElement('b');

    producto.stock < 1 ? stok.textContent = "agotado" : stok.textContent = "Stock: " + producto.stock;
    descripcion.textContent = producto.descripcion;
    precio.textContent = "S/" + producto.precioVenta + ".00";

    descripcionCreado.appendChild(stok);
    descripcionCreado.appendChild(descripcion);
    descripcionCreado.appendChild(precio);

    descripcionCreado.classList.add('descripcion');

    return descripcionCreado;
}



//PAGINACION DE LOS CARDS DE PRODUCTOS

//EVALUAMOS QUE CARDS SE DEBEN MOSTRAR EN LA PAGINA
const showItems = () => {   
    const galeria = listaProducto.children;


    // const galeria = listaProducto.children.cloneNode(true);
    if(galeria != undefined && galeria.length > 0){
        for(let i=0; i < galeria.length; i++ ){
            galeria[i].classList.remove("show");
            galeria[i].classList.add('hide');

            if( i >= (pageNumber * pageSize) - pageSize && i< pageNumber * pageSize ){                
                galeria[i].classList.remove('hide');
                galeria[i].classList.add('show');   

                if( this.parametroBusqueda.value != ""){            
                    if(galeria[i].children[1].children[1].textContent != this.parametroBusqueda.value){
                        console.log(galeria[i].children[i])
                        galeria[i].classList.remove("show");
                        galeria[i].classList.add('hide');
                    }                  
                }        
            }        
        } 
        this.page.textContent = "Page ";
        this.page.textContent += pageNumber;
    }
}

const nextPage = () => {      
    pageNumber++;   
    checkPaginador();
    showItems();
}

const prevPage = () => {
    pageNumber--;
    checkPaginador();
    showItems();
}

const checkPaginador = () => {
   
    let paginacion = Math.ceil(this.storeProducto.length / pageSize);
   
    if(pageNumber == paginacion){
        botonNext.classList.add('disabled');
    }else{
        botonNext.classList.remove('disabled');
    }

    if(pageNumber == 1){
        botonPrev.classList.add('disabled');
    }else{
        botonPrev.classList.remove('disabled');
    }
}







